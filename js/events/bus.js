class EventBus {
    bus;

    constructor() {
        this.bus = [];
    }
    
    registerEvent(name) {
        this.bus.push({name: name, functions: []});
    }
    
    subscribe(name, functionRef, args) {
        this.bus.forEach(function(event) {
            if (name === event.name) {
                return event.functions.push(functionRef);
            }
        });
    }

    unsubscribe(subscription) {
        this.bus.forEach(function(event) {
            if (name === event.name) {
                event.functions = event.functions.filter(x => {
                    console.log(x);
                });
            }
        });
    }
    
    publish(eventName, args) {
        this.bus.forEach(function(event) {
            if (eventName === event.name) {
                event.functions.forEach(function(func) {
                    func(args);
                })
            }
        });
    }
}
