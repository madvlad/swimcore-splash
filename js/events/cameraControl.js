document.addEventListener("keydown", onDocumentKeyDown, false);
document.addEventListener("wheel", onMouseWheel, false);
cameraTweenX = undefined;
cameraTweenY = undefined;
isTweening = false;
waitingActions = []

// Rotate
function onDocumentKeyDown(event) {
    var keyCode = event.which;
    if (keyCode == 40) {
        zoomInWithBounds();
    } else if (keyCode == 38) {
        zoomOutWithBounds();
    }
};

// Zoom
function onMouseWheel(event) {
    if (event.deltaY > 0) {
        zoomOutWithBounds();
    } else {
        zoomInWithBounds();
    }
}

function zoomInWithBounds() {
    if (camera.position.z < 5) {
        camera.position.z += 0.1;
    }
}

function zoomOutWithBounds() {
    if (camera.position.z > 3 ) {
        camera.position.z -= 0.1;
    }
}

function tweenCamera() {
    if (isTweening && Math.round(camera.position.x) === cameraTweenX && Math.round(camera.position.y) === cameraTweenY) {
        isTweening = false;
        bus.publish("moveComplete");
        resolveActions();
        return;
    }

    if (Math.round(camera.position.x) !== cameraTweenX) {
        if (cameraTweenX > camera.position.x) camera.position.x += 0.05;
        if (cameraTweenX < camera.position.x) camera.position.x -= 0.05;
    }

    if (Math.round(camera.position.y) !== cameraTweenY) {
        if (cameraTweenY > camera.position.y) camera.position.y += 0.05;
        if (cameraTweenY < camera.position.y) camera.position.y -= 0.05;
    }
}

function onClick( event ) {
    var offset = this.getClientRects()[0];
    mouse.x = ((event.clientX - offset.left) / 640) * 2- 1;
    mouse.y = - ((event.clientY - offset.bottom) / 480) * 2- 1;

    raycaster.setFromCamera( mouse, camera );

	var intersects = raycaster.intersectObjects( scene.children );

	for ( var i = 0; i < intersects.length; i++ ) {
        var currentObject = intersects[i].object;
        if (currentObject.tag === "ground") {
            isTweening = true;
            cameraTweenX = currentObject.position.x;
            cameraTweenY = currentObject.position.y - 2;

            bus.publish("move", {x: currentObject.position.x, y: currentObject.position.y});
            break;
        } else {
            waitingActions.push({name: "receiveMessage", sceneObject: currentObject, data: currentObject.gameObject.message ? currentObject.gameObject.message : "", remove: false});
        }

        if (currentObject.gameObject && currentObject.gameObject.tag === "item") {
            waitingActions.push({name: "receiveItem", sceneObject: currentObject, data: currentObject.gameObject, remove: true});
        }
    }
}

function resolveActions() {
    waitingActions.forEach(action => {
        bus.publish(action.name, action.data);
        if (action.remove) scene.remove(action.sceneObject);
    });
    waitingActions = [];
}