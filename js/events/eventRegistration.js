function registerEvents() {
    // Player Subscriptions
    bus.registerEvent("move");
    bus.registerEvent("receiveItem");
    bus.registerEvent("moveComplete");

    bus.subscribe("move", player.move.bind(player));
    bus.subscribe("receiveItem", player.getItem.bind(player));

    // HUD Subscriptions
    bus.registerEvent("receiveMessage");
    bus.subscribe("receiveMessage", gameManager.receiveMessage.bind(gameManager));
}