class Player {
    inventory = [];
    positionX = 0;
    positionY = 0;

    constructor(x, y) {
        this.positionX = x;
        this.positionY = y;
    }

    move(newPosition) {
        this.positionX = newPosition.x;
        this.positionY = newPosition.y;
    }

    getItem(item) {
        this.inventory.push(item);
        
        var node = document.createElement("div"); 
        var textNode = document.createTextNode(item.type);

        node.appendChild(textNode);
        document.getElementById("inventoryPanel")
            .appendChild(node);
    }
}