class GameManager {
    messageBox;
    player;

    constructor(messageBox, player) {
        this.messageBox = messageBox;
        this.player = player;
    }

    receiveMessage(message) {
        this.messageBox.innerText = message;
    }
}