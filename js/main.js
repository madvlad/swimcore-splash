var scene = new THREE.Scene();
scene.background = new THREE.Color( 0x0000aa )

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var bus = new EventBus();

var renderer = new THREE.WebGLRenderer();
renderer.setSize( 640, 480 );
renderer.domElement.addEventListener('click', onClick, false );
document.getElementById("3dView").appendChild( renderer.domElement );

setupCamera();
buildEarth();
buildObjects();

var player = new Player(0, -2);

var gameManager = new GameManager(
    document.getElementById("hudMessage"),
    player
);

registerEvents();

function animate() {
    requestAnimationFrame( animate );
    tweenCamera();
    renderer.render( scene, camera );
}

animate();