var camera = new THREE.PerspectiveCamera( 45, 640 / 480, 0.1, 1000 );

function setupCamera() {
    camera.position.z = 4;
    camera.position.y = -4;
    camera.rotation.x = deg(25);

    light = new THREE.AmbientLight(0xdddddd);
    
    scene.add(light);
}