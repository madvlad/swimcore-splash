var plots = [];
var objectList = [];

function getMaterial(hexCode) {
    return new THREE.MeshLambertMaterial( { color: hexCode } )
}

function addBit(x, y, z, color) {
    var geometry = new THREE.PlaneGeometry( 1, 1 );
    var material = color;
    var plot = new THREE.Mesh( geometry, material );
    plot.position.x = x;
    plot.position.y = y;
    plot.position.z = z;
    plot.receiveShadow = true;
    plots.push(plot);
    var sceneChildren = scene.add(plot).children;
    sceneChildren[sceneChildren.length - 1].tag = "ground";
}

function buildEarth() {
    earth.forEach(bit => {
        addBit(bit.x, bit.y, bit.z, getMaterial(parseInt("0x" + bit.color)));
    });
}

function getTexture(objectType) {
    var textureSource = 'textures/';
    switch (objectType) {
        case 'tree':
            textureSource += 'nature/tree.png';
            break;
        case 'wall_stone':
            textureSource += 'wall/stone.jpg';
            break;
        case 'door_stone':
            textureSource += 'wall/stone_door.png';
            break;
        case 'window_stone':
            textureSource += 'wall/stone_window.png';
            break;
        case 'fence_wood':
            textureSource += 'wall/wood_fence.png';
            break;
        case 'decal_splash':
            textureSource += 'decal/splash.png';
            break;
        case 'old_man':
            textureSource += 'people/old_man.png';
            break;
        case 'apple':
            textureSource += 'items/apple.png';
            break;
        default: 
            textureSource += 'error.png';
            break;
    }
    return new THREE.TextureLoader().load( textureSource );
}

function buildObjects() {
    objects.forEach(object => {
        var objectGeometry = new THREE.PlaneGeometry(1,1);
        var objectTexture = getTexture(object.type);
        objectTexture.wrapS = THREE.RepeatWrapping;
        if (object.flip) objectTexture.repeat.x = -1;
        var objectMaterial = new THREE.MeshLambertMaterial( { map: objectTexture, transparent: true, side: THREE.DoubleSide } );
        var objectPlane = new THREE.Mesh(objectGeometry, objectMaterial);
        objectPlane.castShadow = true;

        objectPlane.rotation.x = deg(90);
        objectPlane.rotation.y = object.rot;
        objectPlane.position.x = object.x;
        objectPlane.position.y = object.y;
        objectPlane.position.z = object.z;

        objectList.push(objectPlane);
        var sceneChildren = scene.add(objectPlane).children;
        sceneChildren[sceneChildren.length - 1].gameObject = object;
    });
}